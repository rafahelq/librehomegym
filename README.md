# LIBRE HOME GYM
<p align="center">
  <img title='Logo Libre Home Gym'
       src="https://gitlab.com/podcastlinux/librehomegym/-/raw/main/images/LHG.png"
       width="300"
       height="300"/>
</p>


Un proyecto para crear audios de rutinas de ejercicios físicos en tu propia casa.

## **SOFTWARE LIBRE UTILIZADO:**

### **Audacity**

### **LMMS**

### **Festival**

### **Hispavoces**

### **Inkscape**

## **ANTES DE EMPEZAR (OBLIGATORIA LECTURA):**

Debes tener claro que antes de iniciar cualquier ejercicio físico debes comprobar tu estado de salud.  
Ten en cuenta que:
+ Debes realizar un chequeo médico anual para conocer tu estado de salud.
+ Asegúrate que conoces cómo se realizan correctamente los ejercicios. Contacta con profesionales si necesitas asesorarte en este sentido. 
+ Si notas algún tipo de malestar o lesión, no sigas haciendo estos ejercicios y ponte en manos de profesionales de la salud.


## **QUÉ ES ENTRENAMIENTO A INTERVALOS DE ALTA INTENSIDAD (HIIT):**
El entrenamiento de intervalos de alta intensidad (HIIT, por sus siglas en inglés), es un entrenamiento de fuerza-resistencia, ya que combina ejercicios anaeróbicos y aeróbicos. Habitualmente las sesiones HIIT pueden variar de entre 4 y 30 minutos. Estos cortos e intensos entrenamientos proporcionan capacidad atlética y condición mejoradas, alto metabolismo de glucosa y elevada quema de lípidos (grasa).

Las sesiones de ejercicio HIIT generalmente consisten en un período de calentamiento, varios ejercicios de alta intensidad y finalmente, un periodo de estiramientos. Cada ejercicio se realiza a la máxima potencia posible en un corto espacio de tiempo. Tras ello hay un breve periodo de recuperación que se utiliza para prepararse al siguiente ejercicio. Por ejemplo: 30/10 (30 segundos de ejercicio y 10 segundos de recuperación). 

Con este entrenamiento se trabaja a la vez la potencia, la fuerza, la explosividad y el desarrollo cardiovascular.

## **MATERIAL:**
Esterilla  
Banqueta/silla  

## **GLOSARIO DE EJERCICIOS:**

### **Tren superior:**
Pectoral:  
+ Flexiones
+ Flexiones con rotación
Espalda:
+ Superman

Hombros:  
+ Plancha dinámica
+ Plancha lateral

Brazos:  
+ Tríceps


### **Core:**
+ Abdominales tocando talones
+ Abdominales con pies en alto
+ Plancha estática
+ Escalador de montaña


### **Tren inferior:**

Glúteos:  
+ Elevación de pelvis

Piernas:  
+ Sentadillas
+ Sentadillas con salto
+ Skipping 
+ Skipping de glúteos
+ Movimiento del patinador
+ Tijeras
+ Escaleras

### **Completos: (Tren superior + Tren inferior)**

Burpee:

Jumping Jacks:

## **REALIZACIÓN DE LAS RUTINAS:**

Calentamiento: Antes de iniciar la rutina, realiza 5 minutos de calentamiento realizando movimientos de brazos, piernas, espalda y cuello.  

Rutina específica: Realiza la rutina.

Estiramiento: Una vez termines, realiza estiramientos en el suelo de brazos, piernas, espalda y cuello.

## **RUTINAS:**
### Nivel 1: Iniciación:
7 min: [Audio](https://archive.org/download/librehomegym/7MinLibreHomeGym.mp3)


### Nivel 2: Medio:
10 min: [Audio](https://archive.org/download/librehomegym/10MinLibreHomeGym.mp3)


### Nivel 3: Avanzado:
15 min:


## Licencia

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
